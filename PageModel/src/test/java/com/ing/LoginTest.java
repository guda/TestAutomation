package com.ing;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.page.HomePage;
import com.page.LoginPage;

public class LoginTest {

	
	WebDriver driver;
	WebDriverWait wait; 
	LoginPage objLogin;
	HomePage objHomePage;

	@BeforeTest
	public void setup(){
		String chromepath= System.getProperty("user.dir") + "//src//test//java//Resources//chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", chromepath);
		driver = new ChromeDriver();
		
		
		//System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "//src//test//java//Resources//geckodriver.exe");
		//WebDriver driver = new FirefoxDriver();
		//driver.get("http://www.toolsqa.com");
		
	    //driver = new FirefoxDriver();
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    driver.get("http://mail.google.com/mail");
	}
	
	/**
	 * This test go to http://mail.gmail.com/
	 * Verify login page title as guru99 bank
	 * Login to application
	 * Verify the home page using Dashboard message
	 * @throws IOException 
	 */
	
	@Test(priority=0)
	public void test_HomePage() throws IOException{
		
	    //POM of Login Page
		objLogin = new LoginPage(driver);
		////POM of Home Page
		objHomePage = new HomePage(driver);
				
		
		String strPropertiesFile=System.getProperty("user.dir") + "\\src\\test\\java\\Resources\\";
		File file = new File(strPropertiesFile + "test.properties.txt");
		FileInputStream fileInput = new FileInputStream(file);
		Properties properties = new Properties();
		properties.load(fileInput);
		fileInput.close();
		
		String strUsername=properties.getProperty("username");
		System.out.println("Username()" + strUsername);
		
		String strPassword=properties.getProperty("password");
		System.out.println("Password()" + strPassword);
		
		//=================Login Operation=================
			
		//objLogin.loginToGmail(strUsername, strPassword);
		wait =new WebDriverWait(driver,10);
		
		objLogin.getUserName().sendKeys(strUsername);
		objLogin.getBtnNext().click();
		
		objLogin.getPassword().sendKeys(strPassword);
		objLogin.getBtnNext().click();
		
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		WebElement web1=wait.until(ExpectedConditions.visibilityOf(objHomePage.getBtncomposeName()));
		String strcompose = web1.getText();
		
		System.out.println("Home Page() "  + strcompose);
		
		//Verify home page
		Assert.assertTrue(strcompose.toLowerCase().contains("compose"));
	}
	
	@AfterTest
	public void teardown(){			
	    driver.quit();
	    driver = null;
	}	
}