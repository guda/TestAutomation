package com.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    WebDriver driver;
    
    @FindBy(className="z0")
    WebElement btncomposeName;

    public HomePage(WebDriver driver){
        this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }   

    public WebElement getBtncomposeName() {
		return btncomposeName;
	}

	public void setBtncomposeName(WebElement btncomposeName) {
		this.btncomposeName = btncomposeName;
	}

	//Get the User name from Home Page
    public String getcomposeButtonName(){
         return btncomposeName.getText();
    } 
    
    
}
