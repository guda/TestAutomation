package com.page;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
	
	 	WebDriver driver;

	    @FindBy(id="identifierId")
	    WebElement userName;	    
	    
	    @FindBy(className="CwaK9")
	    WebElement btnNext;

	    @FindBy(name="password")
	    WebElement password;
	   
	    

	    public LoginPage (WebDriver driver){
	        //This initElements method will create all WebElements
	        this.driver = driver;
	        PageFactory.initElements(driver, this);
	    }

	    
	    //Set user name in text box
	    public void setUserName(String strUserName){
	        userName.sendKeys(strUserName);
	    }    

	    
	    //Set password in password text box
	    public void setPassword(String strPassword){
	    	password.sendKeys(strPassword);
	    }	    
	    

	    //Click on login button
	    public void clickNext(){
	    	btnNext.click();
	    }

	    /**
	     * This POM method will be exposed in test case to login in the application
	     * @param strUserName
	     * @param strPasword
	     * @return
	     */

	    public void loginToGmail(String strUserName,String strPasword){
	    	
//	    	By btnreload = By.id("reload-button");
//	    	boolean reload=driver.findElement(btnreload).isDisplayed();	    	
//	        //Enter user name
//	    	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//	    	System.out.println("Check for reload button " + reload);
	    	
	    	
	    	this.setUserName(strUserName);
	        //driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

	        //click next button
	        this.clickNext();
	        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	        
	        //Set the password
	        this.setPassword(strPasword);
	        WebDriverWait ws = new WebDriverWait(driver, 3);
	        ws.until(ExpectedConditions.visibilityOf(btnNext));
	        
	        //Click Login button
	        this.clickNext();
	        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	    }


		public WebElement getUserName() {
			return userName;
		}


		public void setUserName(WebElement userName) {
			this.userName = userName;
		}


		public WebElement getBtnNext() {
			return btnNext;
		}


		public void setBtnNext(WebElement btnNext) {
			this.btnNext = btnNext;
		}


		public WebElement getPassword() {
			return password;
		}


		public void setPassword(WebElement password) {
			this.password = password;
		}

}
